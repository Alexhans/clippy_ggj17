﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class DetectWin : MonoBehaviour {

    public delegate void LevelWinAction(int instanceId, Transform winPosition);
    public static event LevelWinAction OnLevelWon;
    private Animator clippysAnimator;
    public GameObject winEffect;
    public GameObject winCamera;

    void OnTriggerEnter(Collider other) {
        ClippyControl control = other.gameObject.GetComponent<ClippyControl>();
        clippysAnimator = other.GetComponent<Animator>();
        if (control != null && control.isPlayer) {
            if (OnLevelWon != null) {
                OnLevelWon(other.gameObject.GetInstanceID(), gameObject.transform);
                winEffect.SetActive(true);
                winCamera.SetActive(true);
            }


            if (clippysAnimator != null) clippysAnimator.SetTrigger("winTrigger");
            else Debug.LogError("FUCK");
        }
    }
}
