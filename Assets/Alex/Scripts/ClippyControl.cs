﻿using UnityEngine;
using System.Collections;

//[RequireComponent(typeof(Rigidbody))]
public class ClippyControl : MonoBehaviour {
    public delegate void GetBackUpAction();
    public static event GetBackUpAction OnGetBackUp;

    public delegate void UseScreamInFlightAction(float speed, float y_velocity, bool isGoingUp);
    public static event UseScreamInFlightAction OnUseScreamInFlight;

    private int reboundsCounter = 0;
    [SerializeField]
    private Rigidbody myRigidBody;
    private CameraSwitch cameraSwitch;
    public bool isPlayer;
    [SerializeField]
    private float forceStrength = 10.0f;
    public float ForceStrength { get { return forceStrength; } set { forceStrength = value; } }

    [SerializeField]
    private bool decreaseBounceOverTime = false;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private bool debug_on = false;


    private bool finishedFlight = true;
    //private int usedScreamInFlight = 0;
    private bool usedScreamUp = false;
    private bool usedScreamDown = false;
    [SerializeField]
    float reboundFactor = 10.0f;
    private bool debubIsLevelWon;

    [SerializeField]
    private float speedAtWhichITrigger = 14.0f;

    void OnEnable() {
        DebugTogglePhysics.OnToggledPhysics += HandleTogglePhysics;
        DebugTogglePhysics.OnAppliedForce += HandleApplyForce;
        DebugTogglePhysics.OnAppliedSpring += HandleApplySpringForce;
        DebugTogglePhysics.OnCompressingSpring += HandleCompressing;

        DetectWin.OnLevelWon += HandleLevelWon;
    }


    void OnDisable() {
        DebugTogglePhysics.OnToggledPhysics -= HandleTogglePhysics;
        DebugTogglePhysics.OnAppliedForce -= HandleApplyForce;
        DebugTogglePhysics.OnAppliedSpring -= HandleApplySpringForce;
        DebugTogglePhysics.OnCompressingSpring -= HandleCompressing;

        DetectWin.OnLevelWon -= HandleLevelWon;
    }

    // Use this for initialization
    void Awake() {
        myRigidBody = gameObject.GetComponent<Rigidbody>();
        if (!myRigidBody) {
            Debug.LogError("Cant find rigidbody");
        }
    }
    void Start() {
        if (isPlayer) { // DONT MOVE FROM START
            GameObject go = GameObject.Find("Camera");
            cameraSwitch = go.GetComponentInChildren<CameraSwitch>();
        }
    }

    void FixedUpdate() {
        if (isPlayer) {
            animator.SetFloat("y_velocity", myRigidBody.velocity.y);
            //Debug.Log("Magnitude = " + myRigidBody.velocity.magnitude.ToString());
            if (myRigidBody.velocity.magnitude > speedAtWhichITrigger && !finishedFlight) {
                if (!usedScreamUp) {
                    if (myRigidBody.velocity.y > 0.3f) {
                        if (OnUseScreamInFlight != null) OnUseScreamInFlight(myRigidBody.velocity.magnitude, myRigidBody.velocity.y, true);
                        usedScreamUp = true;
                    }
                }
                if (!usedScreamDown) {
                    if (myRigidBody.velocity.y < -0.1f) {
                        if (OnUseScreamInFlight != null) OnUseScreamInFlight(myRigidBody.velocity.magnitude, myRigidBody.velocity.y, false);
                        usedScreamDown = true;
                    }
                }
            }
        }
    }

    private void SetInUprightPosition() {
        //Debug.Log("SetInUprightPosition");
        if (isPlayer && cameraSwitch && !debubIsLevelWon) cameraSwitch.SwitchToCasting();
        animator.SetTrigger("getBackUpTrigger");
        if (isPlayer) {
            if (OnGetBackUp != null) OnGetBackUp();
            finishedFlight = true;
            usedScreamDown = false;
            usedScreamUp = false;
            reboundsCounter = 0;
        }
        //rigidBody.gameObject.transform.position = new Vector3(transform.position.x, 1.0f, transform.position.z);
        //rigidBody.gameObject.transform.rotation = new Quaternion();
    }
    private void HandleTogglePhysics(bool physicsOn) {
        myRigidBody.isKinematic = !physicsOn;
        if (!physicsOn) {
            SetInUprightPosition();
        }
    }

    public void HandleTurnOffPhysics() {
        HandleTogglePhysics(physicsOn: false);
    }

    public void HandleTurnOnPhysics() {
        if (isPlayer && cameraSwitch) cameraSwitch.SwitchToFlying();
        HandleTogglePhysics(physicsOn: true);
    }

    private void HandleForceFuckThis(Vector3 vec) {
        Rigidbody rig = gameObject.GetComponent<Rigidbody>();
        if (rig != null) rig.angularVelocity = Vector3.zero;
        HandleTurnOnPhysics();
        Debug.DrawRay(gameObject.transform.position, vec * 10.0f, Color.yellow);

        finishedFlight = false;
        myRigidBody.AddForce(vec, ForceMode.Impulse);
        //cameraSwitch.SwitchToFlying ();
    }


    public void HandleApplySpringForce(Vector3 direction, float power) {
        //Debug.Log("HandleApplySpringForce Direction" + direction.ToString());
        //Debug.Log("HandleApplySpringForce power " + power.ToString());
        direction = direction.normalized;
        ForceStrength = power;
        HandleForceFuckThis(direction * ForceStrength);
    }

    public void HandleApplySpringForce(float power) {
        ForceStrength = power;
        HandleApplySpringForce(myRigidBody.gameObject.transform.up, ForceStrength);
    }

    public void HandleApplyForce() {
        HandleApplySpringForce(forceStrength);
    }


    public void HandleCompressing(bool set) {
        if (isPlayer) {
            if (animator != null) animator.SetBool("compressing", set);
        }
    }


    public void HandleLevelWon(int instanceId, Transform winPosition) {
        //Debug.Log("Handling win");
        //if (gameObject.GetInstanceID() == instanceId) {
        //HandleTogglePhysics(false);
        //gameObject.transform.position = winPosition.position;
        //gameObject.transform.rotation = winPosition.rotation;
        //}
        debubIsLevelWon = true;
    }

    void OnCollisionEnter(Collision collision) {
        bool hitTopOrBottom = false;
        foreach (ContactPoint contact in collision.contacts) {
            GameObject obj = contact.thisCollider.gameObject;
            //Debug.Log("New Object: " + obj.name.ToString());
            //Debug.Log("New Object: " + contact.otherCollider.gameObject.name.ToString());
            //contact.point.
            Vector3 localContactPoint = obj.transform.InverseTransformPoint(contact.point);
            Vector3 localCenter = obj.transform.InverseTransformPoint(obj.transform.position);
            localContactPoint = new Vector3(0.0f, localContactPoint.y, 0.0f);
            localCenter.Set(localCenter.x, localCenter.y, localCenter.z);
            Debug.DrawLine(obj.transform.TransformPoint(localCenter), obj.transform.TransformPoint(localContactPoint), Color.red);
            float localYDistance = Vector3.Distance(localContactPoint, localCenter);
            float localYDistance2 = -localContactPoint.y + localCenter.y;
            hitTopOrBottom = Mathf.Abs(localYDistance) > 0.5f;

            Debug.DrawRay(contact.point, contact.normal, Color.white);

            if (hitTopOrBottom) {
                Vector3 direction = localCenter - localContactPoint;
                Rigidbody rig = gameObject.GetComponent<Rigidbody>();
                if (debug_on && isPlayer) {
                    Debug.Log("Hit Top or bottom " + obj.name.ToString());
                    Debug.Log("Velocity: " + rig.velocity.ToString());
                    Debug.Log("magnitude: " + rig.velocity.magnitude.ToString());
                }
                ForceStrength = rig.velocity.magnitude;
                if (decreaseBounceOverTime) {
                    ForceStrength *= reboundFactor * Mathf.Max(0.0f, 1.0f - (0.1f * reboundsCounter));
                } else { ForceStrength *= reboundFactor; }

                if (debug_on && isPlayer) {
                    Debug.Log("Force " + ForceStrength.ToString());
                    Debug.Log("reboundsCounter " + reboundsCounter.ToString());
                }
                reboundsCounter += 1;
                //HandleApplySpringForce(ForceStrength);
                HandleApplySpringForce(direction, ForceStrength);
                if (debug_on && isPlayer) {
                    Debug.Break();
                }
            }

            if (debug_on && isPlayer) {
                Debug.Log(localYDistance2.ToString() + " == " + localYDistance.ToString());
                Debug.Log("Are local distances equal: " + (localYDistance == localYDistance2).ToString());
            }
            //GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //sphere.transform.position = obj.transform.TransformPoint(localCenter);
            /*
            Debug.Log("localContactPoint: " + localContactPoint.ToString());
            Debug.Log("localCenter: " + localCenter.ToString());
            Debug.Log("Contact Point:      " + contact.point.ToString());
            Debug.Log("Transform Position: " + obj.transform.position.ToString());
            */

            if (debug_on && isPlayer) {
                Debug.Break();
            }
            if (hitTopOrBottom) {
                break;
            }
        }
        if (isPlayer && (animator != null)) {
            animator.SetTrigger("collideTrigger");
        }
    }
}
