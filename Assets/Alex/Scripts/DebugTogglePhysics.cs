﻿using UnityEngine;
using System.Collections;

public class DebugTogglePhysics : MonoBehaviour {

    public delegate void TogglePhysicsAction(bool set);
    public static event TogglePhysicsAction OnToggledPhysics;

    public delegate void ApplyForceAction();
    public static event ApplyForceAction OnAppliedForce;

    public delegate void ApplySpringAction(float power);
    public static event ApplySpringAction OnAppliedSpring;

    //public delegate void LevelStartAction();
    //public static event LevelStartAction OnLevelStarted;

    public delegate void LevelWinAction();
    public static event LevelWinAction OnLevelWon;

    public delegate void CompressSpringAction(bool set);
    public static event CompressSpringAction OnCompressingSpring;

    //private bool physicsOn = true;

    [SerializeField]
    private float maxPower = 10.0f;
    [SerializeField]
    private float power = 0.0f;
    /*
    [SerializeField]
    [Header("Speed Factor for power gathering")]
    [Tooltip("Speed factor at which power of launch loads")]
    */
    //private float barFactor = 3.0f;
    private const float springCompressionAnimationDuration = 2.0f;

    //[SerializeField]
    //private float rotation = 0.0f;

    [Header("Replace with label or something")]
    [SerializeField]
    private float debug_last_know_power = 0.0f;

    private float CalculateNewPower(float timeDiff) {
        if (timeDiff <= 0.0f || !temp_key_down) {
            //Debug.Log("Return last power since timediff isn't valid " + power.ToString());
            return 0.0f;
        }
        float res = (maxPower * timeDiff) / springCompressionAnimationDuration;
        res = System.Math.Min(res, maxPower);
        return res;

        //float cant = springCompressionAnimationDuration / Time.deltaTime;
        //barFactor = maxPower / cant;
        //Debug.Log("cant: " + cant.ToString());
        //float res = System.Math.Min((barFactor * Time.time) - power, maxPower);
        //float res = System.Math.Min(barFactor + power, maxPower);
        //Debug.Log("-------------");
        //Debug.Log("Time time: " + Time.time);
        //Debug.Log("bar factor: " + barFactor);
        //Debug.Log("delta time: " + Time.deltaTime.ToString());
        //Debug.Log("calc: " + (barFactor * Time.time).ToString());
        //Debug.Log("power before max limit: " + res.ToString());
        //Debug.Log("max power: " + (maxPower).ToString());
        //Debug.Log("power: " + res.ToString());
        //  Debug.Break();
    }

    public void TogglePhysicsFalse() {
        if (OnToggledPhysics != null) {
            OnToggledPhysics(false);
        }
    }
    float clickDownTime = 0.0f;
    float clickReleaseTime = 0.0f;
    private bool temp_key_down = false;
    // Update is called once per frame
    void Update() {
        /*
        if (Input.GetKeyDown(KeyCode.T)) {
            if (OnToggledPhysics != null) {
                physicsOn = !physicsOn;
                OnToggledPhysics(physicsOn);
            }
        }
        */

        if (Input.GetKeyDown(KeyCode.Space)) {
            TogglePhysicsFalse();
        }
        /*if (Input.GetKeyDown(KeyCode.A)) {
            if (OnAppliedForce != null) {
                OnAppliedForce();
            }
        }
        */
        /*
        if (Input.GetKeyDown(KeyCode.G)) {
            if (OnLevelWon != null) {
                OnLevelWon();
            }
        }
        if (Input.GetKeyDown(KeyCode.H)) {
            if (OnLevelStarted != null) {
                OnLevelStarted();
            }
        }
        */

        float cachedPower = CalculateNewPower(clickReleaseTime - clickDownTime);
        clickReleaseTime = Time.time;

        // Gather power logic
        if (Input.GetButtonDown("Fire1")) {
            //Debug.Log("Fire Down");
            power = Time.time;
            clickDownTime = Time.time;
            temp_key_down = true;
            if (OnCompressingSpring != null) OnCompressingSpring(true);
        }
        if (Input.GetButtonUp("Fire1")) {
            //Debug.Log("Fire UP");
            power = cachedPower;

            //Debug.Log(System.String.Format("Apply power: {0} vector: {1}", power, transform.forward * power));
            if (OnToggledPhysics != null) OnToggledPhysics(true);
            if (OnCompressingSpring != null) OnCompressingSpring(false);
            if (OnAppliedSpring != null) OnAppliedSpring(power);
            temp_key_down = false;
            power = 0.0f;
            debug_last_know_power = 0.0f;
            clickReleaseTime = 0.0f;
            clickDownTime = 0.0f;
        }
        if (temp_key_down) {
            debug_last_know_power = cachedPower;
        }

    }

}
