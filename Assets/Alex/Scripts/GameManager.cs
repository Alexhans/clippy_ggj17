﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace GameNS {
    public enum LevelStatus {
        Win,
        Start,
        Playing
    }

    //[RequireComponent(typeof(AudioListener))]
    //[RequireComponent(typeof(AudioSource))]
    public class GameManager : MonoBehaviour {
        public static GameManager i;

        [SerializeField]
        private AudioSource myAudioSource;
        [SerializeField]
        private AudioSource myScreamAudioSource;


        private Dictionary<string, AudioClip> comp = new Dictionary<string, AudioClip>();

        void OnEnable() {
            DebugTogglePhysics.OnToggledPhysics += HandleTogglePhysics;
            DebugTogglePhysics.OnAppliedForce += HandleApplyForce;
            DebugTogglePhysics.OnAppliedSpring += HandleApplySpringForce;
            DebugTogglePhysics.OnCompressingSpring += HandleCompressing;
            ClippyControl.OnUseScreamInFlight += HandleScreaming;
            ClippyControl.OnGetBackUp += HandleGetBackUp;

            DetectWin.OnLevelWon += HandleLevelWon;
        }

        void OnDisable() {
            DebugTogglePhysics.OnToggledPhysics -= HandleTogglePhysics;
            DebugTogglePhysics.OnAppliedForce -= HandleApplyForce;
            DebugTogglePhysics.OnAppliedSpring -= HandleApplySpringForce;
            DebugTogglePhysics.OnCompressingSpring -= HandleCompressing;
            ClippyControl.OnGetBackUp -= HandleGetBackUp;
            ClippyControl.OnUseScreamInFlight -= HandleScreaming;

            DetectWin.OnLevelWon -= HandleLevelWon;
        }

        private void LoadOneSoundToDic(string sound_name) {
            string audioClipName = sound_name;
            AudioClip tempAudioClip = Resources.Load(audioClipName) as AudioClip;
            if (!tempAudioClip) {
                Debug.LogError("coulnd't load Audio");
            }
            //myAudioSource.clip = tempAudioClip;
            //Debug.Log(tempAudioClip.loadState);
            comp.Add(audioClipName, tempAudioClip);
        }
        private void LoadSounds() {
            if (comp.Count > 0) return;
            LoadOneSoundToDic("compression");
            LoadOneSoundToDic("release_spring");
            LoadOneSoundToDic("ay");
            LoadOneSoundToDic("collide");
            LoadOneSoundToDic("fly_down");
            LoadOneSoundToDic("fly_down_2");
            LoadOneSoundToDic("fly_up_1");
            LoadOneSoundToDic("fly_up_2");
            LoadOneSoundToDic("go_back");
            LoadOneSoundToDic("relief");
            LoadOneSoundToDic("yuju");
            LoadOneSoundToDic("win_song");
        }

        void Awake() {
            CreateClassSingleton();
            LoadSounds();
        }

        private void HandleLevelStart(int instanceId, Transform winPosition) {
        }

        private void HandleLevelWon(int instanceId, Transform winPosition) {
            PlayOneShotClip("relief");
            PlayOneShotClip("win_song");
        }
        //private float speedThresholdToShout = 1.0f;

        IEnumerator WaitAndPrint() {
            print("Start corout " + Time.time);
            yield return new WaitForSeconds(0.2f);
            print("is_audio_playing " + myAudioSource.isPlaying);
            PlayOneShotClip("fly_up_2");
            print("WaitAndPrint " + Time.time);
        }

        [SerializeField]
        private float screamUpVolume = 0.3f;
        [SerializeField]
        private float screamDownVolume = 0.15f;

        private void HandleScreaming(float speed, float y_speed, bool isGoingUp) {
            //Debug.Log("speed: " + speed.ToString());
            if (!isGoingUp) {
                //Debug.Log("scream down");
                PlayOneShotClip("fly_down_2", myScreamAudioSource, screamUpVolume);
            } else {

                //Debug.Log("scream up");

                //Debug.Log("Length: " + myAudioSource.clip.length.ToString());
                //Debug.Log("Audio time: " + myAudioSource.time.ToString());
                //StartCoroutine(WaitAndPrint());
                //myAudioSource.PlayDelayed(1.0f);
                PlayOneShotClip("fly_up_2", myScreamAudioSource, screamDownVolume);
            }
        }

        private void HandleGetBackUp() {
            PlayOneShotClip("go_back");
        }

        private void HandleCompressing(bool set) {
            //Debug.Log("TEST!!!!");

            if (set) {
                PlayOneShotClip("compression");
            } else {
                //Debug.Log("release_spring!!!!");
                PlayOneShotClip("release_spring");
            }
        }

        private void PlayOneShotClip(string name, AudioSource source = null, float vol = 1.0f) {
            if (source == null) {
                source = myAudioSource;
            }
            if (!comp.ContainsKey(name)) {
                Debug.LogError("PlayOneShotClipError");
                return;
            }
            AudioClip clip = comp[name];
            if (clip) {
                source.clip = clip;
                if (clip.loadState == AudioDataLoadState.Loaded) {
                    source.volume = vol;
                    source.Play();
                }
            } else {
                Debug.LogWarning("Horrible");
            }
        }

        private void HandleApplySpringForce(float power) {
        }

        private void HandleApplyForce() {
        }

        private void HandleTogglePhysics(bool set) {
        }



        // Use this for initialization
        void Start() {
            Cursor.visible = false;
        }

        // Update is called once per frame
        void Update() {

        }

        void CreateClassSingleton() {
            if (i == null) {
                i = this;
                DontDestroyOnLoad(gameObject);
                i.LoadSounds();
            } else Destroy(gameObject);
        }
    }
}
