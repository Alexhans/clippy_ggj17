﻿using UnityEngine;
using System.Collections;

static public class NameManager {
    private static ArrayList names =
     new ArrayList(new string[] { "Clippy", "Cloppy", "Cluppy", "Clampy", "Clappy", "Crispy", "Cluppy" });

    static public object ClippyName {
        get {
            return names[Random.Range(0, names.Count - 1)];
        }
    }
}
