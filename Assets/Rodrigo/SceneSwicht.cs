﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class SceneSwicht : MonoBehaviour {

    // Update is called once per frame
    void Update() {
        // Next level
        if (Input.GetKeyDown(KeyCode.Return)) {
            int nextScene = SceneManager.GetActiveScene().buildIndex + 1;
            if (nextScene > SceneManager.sceneCount) {
                ResetScene();
            } else {
                SceneManager.LoadScene(nextScene);
            }
        }
        // Restart
        if (Input.GetKeyDown(KeyCode.P)) {
            int previousScene = SceneManager.GetActiveScene().buildIndex - 1;
            if (previousScene >= 0) SceneManager.LoadScene(previousScene);
        }
        // main menu
        if (Input.GetKeyDown(KeyCode.Q)) {
            SceneManager.LoadScene("MainMenu");
        }
        if (Input.GetKeyDown(KeyCode.R)) {
            ResetScene();
        }
    }

    private static void ResetScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void StartGame() {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
