﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DebugLanguageTest : MonoBehaviour {
    //SystemLanguage lang = SystemLanguage.English;

    [SerializeField]
    private Text text;

    [SerializeField]
    private string spanish_text; 

    void Start() {
        /*
        SystemLanguage temp = Application.systemLanguage;
        if (temp != SystemLanguage.Unknown) {
            lang = temp;
            Debug.Log("Language detected: " + lang);
        }
        */
        ChangeLang();
    }

    void ChangeLang() {
        if (spanish_text == "") Debug.LogError("Missing spanish Text");
        if (text != null) {
            if (Application.systemLanguage == SystemLanguage.Spanish) text.text = spanish_text.Replace("\\n", "\n");
        }
    }
}
