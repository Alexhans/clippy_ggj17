﻿using UnityEngine;
using System.Collections;
using System;

public class CameraSwitch : MonoBehaviour {


    //public GameObject moveToChild;
    public GameObject clippyPrefab;
    private GameObject clippyObject;
    //public bool instantiateOnStart;
    private Transform clippyCamera;
    public Transform clippyInitialPos;
    public Camera second_cam;

    //private Transform initialPos;
    private CameraFollow cameraScript;
    private ClippyControl control;
    [SerializeField]
    private Transform winZoneTransform;

    // Use this for initialization
    public Transform[] launchPositions;

    void Awake() {
        //if (instantiateOnStart) Instantiate (clippyObject);
        //initialPos = transform;
        clippyObject = Instantiate(clippyPrefab, clippyInitialPos.position, clippyInitialPos.rotation, transform) as GameObject;
        control = clippyObject.GetComponent<ClippyControl>();
        control.isPlayer = true;
        cameraScript = GetComponent<CameraFollow>();
        clippyCamera = clippyObject.transform.FindChild("Camera");
        //winZone = GameObject.Find ("WinZone");
    }

    void CalculateDistancesFromPositions() {
        int count = 0;
        float min = float.MaxValue;
        float distance = 0.0f;
        Transform target = null;
        foreach (Transform positions in launchPositions) {
            distance = System.Math.Abs(Vector3.Distance(positions.transform.position, clippyObject.transform.position));
            //Debug.Log(positions.name + " - pos: "+ count.ToString() + " dist: " + distance.ToString() +  " : " + positions.transform.position);
            if (distance < min) {
                min = distance;
                target = positions.transform;
            }
            count += 1;
        }
        //Debug.Log("found distance: " + min.ToString() + " name: " + target.name);
        transform.parent.position = target.position;
        transform.parent.rotation = target.rotation;
        transform.parent.position = new Vector3(
            transform.parent.position.x,
            transform.parent.position.y + 2.0f,
            transform.parent.position.z);
    }

    void Update() {
        /*
		if (Input.GetKeyDown (KeyCode.I)){
			SetCameraBehindClippy ();
		}
		if (Input.GetKeyDown (KeyCode.O)){
			ResetClippy ();
		}
        */
        if (Input.GetKeyDown(KeyCode.C)) {
            ToggleBetweenCams();
        }
    }

    public void ToggleBetweenCams() {
        Camera myCam = GetComponent<Camera>();
        if (myCam != null) {
            bool whoknows = !myCam.enabled;
            myCam.enabled = whoknows;
            second_cam.gameObject.SetActive(!whoknows);
        }
        //bool active = second_cam.gameObject.activeSelf;
    }

    public void SetCameraBehindClippy() {

        Vector3 pos = clippyObject.transform.position;
        pos.y += 2f;
        //transform.parent.position = pos;
        CalculateDistancesFromPositions();
        //Vector3 look = winZoneTransform.position;
        //look = new Vector3(look.x, look.y, look.z + transform.position.z);
        //transform.parent.LookAt(look);
        //transform.parent.LookAt (winZone.transform);
        //SwitchToCasting ();
        //print (transform.parent.position); 
    }

    public void ResetClippy() {
        //control.HandleTurnOffPhysics ();
        clippyObject.transform.parent = transform;
        clippyObject.transform.position = clippyInitialPos.position;
        clippyObject.transform.rotation = clippyInitialPos.rotation;
        clippyCamera.gameObject.SetActive(false);
    }

    public void SwitchToFlying() {
        cameraScript.isCasting = false;
        clippyCamera.gameObject.SetActive(true);
        clippyObject.transform.SetParent(null);
        //transform.SetParent (moveToChild.transform);
    }
    public void SwitchToCasting() {

        cameraScript.isCasting = true;
        SetCameraBehindClippy();
        //transform.SetParent (null);
        //transform.position = initialPos.position;
        //transform.rotation = initialPos.rotation;
        ResetClippy();
        //clippyObject.transform.SetParent (transform);
    }

}
