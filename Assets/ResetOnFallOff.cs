﻿using UnityEngine;
using System.Collections;
using System;

public class ResetOnFallOff : MonoBehaviour {
    [SerializeField]
    private GameObject togglePhysics;

    private void HandleTogglePhysics(bool set) {
        throw new NotImplementedException();
    }

    void OnTriggerEnter(Collider other) {
        DebugTogglePhysics script = togglePhysics.GetComponent<DebugTogglePhysics>();
        script.TogglePhysicsFalse();
    }
}
