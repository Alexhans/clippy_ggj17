﻿using UnityEngine;
using System.Collections;

public class DrawGizmo : MonoBehaviour {

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawCube(transform.position, new Vector3(2, 2, 2));
    }

}
