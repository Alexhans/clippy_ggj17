﻿using UnityEngine;
using System.Collections;

public class DetectWhichSpriteBasedOnLang : MonoBehaviour {

    public GameObject adentro_spa;
    public GameObject adentro_eng;
    // Use this for initialization

    void Start() {
        if (Application.systemLanguage == SystemLanguage.Spanish) {
            if(adentro_eng != null) Destroy(adentro_eng);
        } else {
            if(adentro_spa != null) Destroy(adentro_spa);
        }
    }

}
